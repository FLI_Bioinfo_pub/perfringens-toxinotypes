## _Clostridium perfringens_ toxinotypes  

  
The species _C. perfringens_ can be classified into seven toxinotypes according to differnt combinations of the so-called typing toxins. This repository includes the sequences of the typing toxin genes formatted for use with ABRicate. 

The scheme implemented for the classification has been described by Rood et al., 2018. 

Rood JI, Adams V, Lacey J, Lyras D, McClane BA, Melville SB, Moore RJ, Popoff MR, Sarker MR, Songer JG, Uzal FA, Van Immerseel F. Expansion of the Clostridium perfringens toxin-based typing scheme. Anaerobe. 2018 Oct;53:5-10 | PMID: 29866424 |  [doi: 10.1016/j.anaerobe.2018.04.011](https://www.sciencedirect.com/science/article/pii/S1075996418300684?via%3Dihub).
